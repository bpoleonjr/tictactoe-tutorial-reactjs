## How to run this Tic Tac Toe game

1. To run this project, download the project as a zip.
2. Extract and navigate to the project directory.
3. Run `npm start` in the console.

That command will run the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.
